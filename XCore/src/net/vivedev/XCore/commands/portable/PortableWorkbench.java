package net.vivedev.XCore.commands.portable;

import net.vivedev.XCore.language.LanguageManager;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PortableWorkbench implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player){
            Player p = (Player)sender;
            if(p.hasPermission("xcore.commands.portable-workbench.use")){
                p.openWorkbench(p.getLocation(), true);
                p.playSound(p.getLocation(), Sound.LEVEL_UP, 1, 1);
            }else{
                p.sendMessage(LanguageManager.getPrase("ERROR_PERMISSION_NOT_SET"));
            }
        }else{
            sender.sendMessage(LanguageManager.getPrase("ERROR_INVENTORY_IN_CONSOLE"));
        }
        return false;
    }

}
