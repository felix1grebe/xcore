package net.vivedev.XCore.commands.world;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.vivedev.XCore.XCore;
import net.vivedev.XCore.language.LanguageManager;

public class day implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player)sender;
			if(p.hasPermission("xcore.commands.world-interaction.use")) {
				p.getWorld().setTime(1000);
				System.out.println(LanguageManager.getPrase("CONSOLE_PLAYER_CHANGED_TIME").replaceAll("%p%", p.getName()));
				p.sendMessage(XCore.INSTANCE.prefix + LanguageManager.getPrase("PLAYER_CHANGED_TIME"));
			}else {
				p.sendMessage(LanguageManager.getPrase("ERROR_PERMISSION_NOT_SET"));
			}
		}else {
			sender.sendMessage(LanguageManager.getPrase("ERROR_WORLDCHANGES_IN_CONSOLE"));
		}
		return false;
	}

}
