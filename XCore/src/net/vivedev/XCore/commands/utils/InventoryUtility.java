package net.vivedev.XCore.commands.utils;


import net.minecraft.server.v1_8_R3.*;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.event.CraftEventFactory;
import org.bukkit.entity.Player;

public class InventoryUtility {

    public static void openEnchanter(Player p){
        EntityPlayer e = ((CraftPlayer)p).getHandle();
        openContainer(e, new ContainerEnchantTable(e.inventory, e.world, new BlockPosition(0, 0, 0)), "minecraft:enchanting_table", "");
    }

    public static void openAnvil(Player p){
        EntityPlayer e = ((CraftPlayer)p).getHandle();
        openContainer(e, new ContainerAnvil(e.inventory, e.world, new BlockPosition(0, 0, 0), e), "minecraft:anvil", "");
    }

    public static void openContainer(EntityPlayer p, Container c, String name, String txt){
        Container cont = CraftEventFactory.callInventoryOpenEvent(p, c);
        int nextID = p.nextContainerCounter();

        cont.windowId = nextID;
        cont.addSlotListener(p);
        cont.checkReachable = false;
        p.activeContainer = cont;

        p.playerConnection.sendPacket(new PacketPlayOutOpenWindow(nextID, name, new ChatComponentText(txt)));
    }

}
