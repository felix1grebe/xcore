package net.vivedev.XCore.settings;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.File;
import java.io.FileReader;
import java.io.PrintStream;

public class SettingManagement {

    public File LANGUAGE_FILE;

    // LANGUAGE

    public boolean LANGUAGE_IS_SET () throws Exception{
        if(LANGUAGE_FILE.exists() == false){
            LANGUAGE_FILE.createNewFile();
            return false;
        }else{
            return true;
        }
    }

    public void LANGUAGE_SET_TYPE(String LANGUAGE_PACKAGE) throws Exception{
        if(LANGUAGE_IS_SET() == false){
            JsonObject o = new JsonObject();
            o.addProperty("LANGUAGE", LANGUAGE_PACKAGE);
            LANGUAGE_FILE.createNewFile();
            PrintStream out = new PrintStream(LANGUAGE_FILE);
            out.println(o.getAsString());
            out.close();
        }else{
            JsonObject o = new JsonParser().parse(new FileReader(LANGUAGE_FILE)).getAsJsonObject();
            o.addProperty("LANGUAGE", LANGUAGE_PACKAGE);
            LANGUAGE_FILE.delete();
            LANGUAGE_FILE.createNewFile();
            PrintStream out = new PrintStream(LANGUAGE_FILE);
            out.println(o.getAsString());
            out.close();
        }
    }

    public void LANGUAGE_SET_VERSION(String LANGUAGE_VERSION) throws Exception {
        if(LANGUAGE_IS_SET() == false){
            JsonObject o = new JsonObject();
            o.addProperty("VERSION", LANGUAGE_VERSION);
            LANGUAGE_FILE.createNewFile();
            PrintStream out = new PrintStream(LANGUAGE_FILE);
            out.println(o.getAsString());
            out.close();
        }else{
            JsonObject o = new JsonParser().parse(new FileReader(LANGUAGE_FILE)).getAsJsonObject();
            o.addProperty("VERSION", LANGUAGE_VERSION);
            LANGUAGE_FILE.delete();
            LANGUAGE_FILE.createNewFile();
            PrintStream out = new PrintStream(LANGUAGE_FILE);
            out.println(o.getAsString());
            out.close();
        }
    }

    public String LANGUAGE_GET_TYPE() throws Exception{
        if(LANGUAGE_IS_SET() == true){

            JsonObject o = new JsonParser().parse(new FileReader(LANGUAGE_FILE)).getAsJsonObject();
            String TYPE = o.get("LANGUAGE").getAsString();

            return TYPE;
        }else{
            return "";
        }
    }

    public String LANGUAGE_GET_VERSION() throws Exception{
        if(LANGUAGE_IS_SET() == true){

            JsonObject o = new JsonParser().parse(new FileReader(LANGUAGE_FILE)).getAsJsonObject();
            String VERSION = o.get("VERSION").getAsString();

            return VERSION;
        }else{
            return "";
        }
    }

}
