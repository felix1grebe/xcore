package net.vivedev.XCore;

import java.io.DataInputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

import org.bukkit.plugin.java.JavaPlugin;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import net.vivedev.XCore.commands.portable.PortableAnvil;
import net.vivedev.XCore.commands.portable.PortableEnchantmentTable;
import net.vivedev.XCore.commands.portable.PortableEnderchest;
import net.vivedev.XCore.commands.portable.PortableWorkbench;
import net.vivedev.XCore.commands.world.day;
import net.vivedev.XCore.commands.world.night;
import net.vivedev.XCore.settings.SettingManagement;

public class XCore extends JavaPlugin {

    public static XCore INSTANCE = null;

    public Socket con;
    public PrintStream cout;
    public DataInputStream cin;
    public SettingManagement SETTINGS;
    
    public String prefix = "";

    @Override
    public void onEnable(){
        INSTANCE = this;
        SETTINGS = new SettingManagement();
        System.out.println("[XCore] Please wait reading Settings...");
        try {
            con = new Socket("inf.vivedev.net", 9989);
            cin = new DataInputStream(con.getInputStream());
            cout = new PrintStream(con.getOutputStream());

            if(SETTINGS.LANGUAGE_IS_SET() == false){
                System.out.println("[XCore] Your Language is not set yet!");
                System.out.println("[XCore] Please enter your country's ISO-CODE to continue:");
                String ISO_CODE = "";
                DataInputStream IN = new DataInputStream(System.in);
                do {
                    String data = IN.readLine();

                    if(data != null && !data.equalsIgnoreCase("")){
                        ISO_CODE = data;
                    }


                }while (ISO_CODE == "" || ISO_CODE == null);

                SETTINGS.LANGUAGE_SET_TYPE(ISO_CODE);

                JsonObject o = new JsonObject();
                o.addProperty("PLUGIN", "XCore");
                o.addProperty("ACTION", "LANGUAGE_DOWNLOAD");
                o.addProperty("LANGUAGE", SETTINGS.LANGUAGE_GET_TYPE());

                cout.println(o.getAsString());

                JsonObject DOWNLOAD = new JsonParser().parse(new JsonReader(new InputStreamReader(cin))).getAsJsonObject();

                SETTINGS.LANGUAGE_FILE.delete();
                SETTINGS.LANGUAGE_FILE.createNewFile();
                PrintStream out = new PrintStream(SETTINGS.LANGUAGE_FILE);
                out.println(DOWNLOAD.getAsString());
                out.close();
            }
        }catch (Exception e){

        }

        //      PORTABLE ITEMS

        getCommand("anvil").setExecutor(new PortableAnvil());
        getCommand("pa").setExecutor(new PortableAnvil());
        getCommand("workbench").setExecutor(new PortableWorkbench());
        getCommand("wb").setExecutor(new PortableWorkbench());
        getCommand("enderchest").setExecutor(new PortableEnderchest());
        getCommand("ec").setExecutor(new PortableEnderchest());
        getCommand("enchantmenttable").setExecutor(new PortableEnchantmentTable());
        getCommand("emt").setExecutor(new PortableEnchantmentTable());

        //		WORLD COMMANDS
        
        getCommand("day").setExecutor(new day());
        getCommand("night").setExecutor(new night());

        prefix = this.getConfig().getString("ingame-prefix");
    }

    @Override
    public void onDisable(){

    }

}
