package net.vivedev.XCore.language;

import java.io.FileReader;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import net.vivedev.XCore.XCore;

public class LanguageManager {

    public static String getPrase(String KEY) {
        try {
            JsonObject o = new JsonParser().parse(new JsonReader(new FileReader(XCore.INSTANCE.SETTINGS.LANGUAGE_FILE))).getAsJsonObject();
            String val = o.get(KEY).getAsString();

            return val;
        }catch (Exception e){
            return null;
        }

    }
    

}
